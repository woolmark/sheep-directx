#pragma once

#include <RString.h>
#include <Image.h>

class BMPImage : public Image {
private:
  LPBITMAPINFO lpInfo;
  LPBYTE lpBMP;
  LPBYTE lpBuf;
  int width;
  int height;
public:
  BMPImage(void);
  ~BMPImage(void);
  BMPImage(int width, int height);
  BMPImage(RString string);
  LPBITMAPINFO getBitmapInfo(void);
  LPBYTE getBytes(void);
  int getWidth(void);
  int getHeight(void);
  void release(void);
};
