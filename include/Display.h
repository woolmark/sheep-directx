#pragma once

#include <Canvas.h>
#include <Graphics.h>
#include <RDX.h>

/**
 * Displayクラス。画面の管理、DirectDrawの管理などをするクラス。
 * Displayクラスのコンストラクタはprivateとなっているため、
 * Displayクラスのインスタンスを取得するためには、
 * staticなgetDisplay()関数をコールする。 
 *
 * Display d = Display::getDisplay();
 */
class Display {
private:
	/** 描画キャンバスのポインタ **/
	Canvas* canvas;

	/** DirectX管理オブジェクトのポインタ **/
	RDX* directX;

	/** FullScreenモード用のフラグ **/
	bool fullscreen;

	/** 描画グラフィックスのポインタ **/
	Graphics* g;

	/** 画面の高さ **/
	static int height;

	/** Displayクラスのインスタンス **/
	static Display* instance;

	/** 画面のセカンダリサフェース **/
	LPDIRECTDRAWSURFACE lpBack;

	/** クリッパー **/
	LPDIRECTDRAWCLIPPER lpClipper;

	/** 画面のプライマリサフェース **/
	LPDIRECTDRAWSURFACE lpFront;

	/** オフスクリンサフェース **/
	LPDIRECTDRAWSURFACE lpWork;

	/** 画面の幅 **/
	static int width;

	/**
	 * Displayクラスのコンストラクタ。privateであるため、コンストラクタ
	 * が直接呼ばれることを禁止している。
	 */
	Display(void);
public:
	/** キーボードのキーダウンイベント **/
	static const int KEYBOARD_DOWN;

	/** キーボードのキーアップイベント **/
	static const int KEYBOARD_UP;

	/** マウスの左ボタンアップイベント **/
	static const int LEFT_MOUSE_UP;

	/** マウスの左ボタンダウンイベント **/
	static const int LEFT_MOUSE_DOWN;

	/** マウスの左ボタンダブルクリックイベント **/
	static const int LEFT_MOUSE_DOUBLECLICK;

	/** マウスの右ボタンアップイベント **/
	static const int RIGHT_MOUSE_UP;

	/** マウスの右ボタンダウンイベント **/
	static const int RIGHT_MOUSE_DOWN;

	/** マウスの右ボタンダブルクリックイベント **/
	static const int RIGHT_MOUSE_DOUBLECLICK;

	/**
	 * 描画終了時に、この関数が呼ばれ、バックバッファとセカンダリバッファを
	 * 入れ替えることにより画面が更新される。
	 */
	void endPaint(void);

	/**
	 * Displayクラスのインスタンスを取得する場合は、この関数を呼ぶ。
	 * @return Displayクラスのインスタンスのポインタ
	 */
	static Display* getDisplay(void);

	/**
	 * 画面の高さを取得する関数。
	 * @return 画面高さ
	 */
	static int getHeight(void);

	/**
	 * 画面の幅を取得する関数。
	 * @return 画面幅
	 */
	static int getWidth(void);

	/**
	 * Canvasクラスの初期化開始関数。
	 */
	void initCanvas(void);

	/**
	 * DirectDrawの初期化開始関数。
	 */
	void initDirectDraw(void);

	/**
	 * フルスクリンモードかどうかを示す。
	 * @return フルスクリンモードならtrue,フルスクリンモードでない場合はfalseを返す。
	 */
	bool isFullScreenMode(void);

	/**
	 * キーボードイベントをListener通知するための関数。
	 * @param event イベントのタイプ。
	 * @param vKey  押されたキーの種類。
	 */
	void keyBoardEvent(int event, int vKey);

	/**
	 * マウスイベントをListener通知するための関数。
	 * @param button マウスボタンのタイプとイベントの種類。
	 */
	void mouseEvent(int button);

	/**
	 * Displayクラスのインスタンスを解放するための関数。
	 * Displayクラスが不要になった場合、この関数を呼ばなければならない。
	 */
	void releaseDisplay(void);
	void setCurrent(Canvas* screen);
	void setFullScreenMode(bool mode);
	void setWindowSize(int x, int y);
	void startPaint(HWND hWnd);
};

