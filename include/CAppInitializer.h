#pragma once

class CAppInitializer
{
private:
  static HWND hWnd;
  static HINSTANCE hThisInst;
  static HINSTANCE hPrevInst;
  static LPSTR lpszArgs;
  static int nWinMode;
public:
  static LPSTR getCommandLine(void);
  static HWND getHWND(void);
  static HINSTANCE getInstance(void);
  static HINSTANCE getPrevInstance(void);
  static void init(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow);
  static void setHWND(HWND HWnd);
  static int winMode(void);
};
