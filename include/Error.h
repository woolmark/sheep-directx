#pragma once

class Error {
public:
  static const char* DD_ALREADY;
  static const char* DD_EXE_ALREDY;
  static const char* DD_HWND_ALREDY;
  static const char* DD_HWND_SUBCLASS;
  static const char* DD_INCOMPATIBLEPRIMARY;
  static const char* DD_INVALID_OBJ;
  static const char* DD_INVALID_MODE;
  static const char* DD_INVALIDCAPS;
  static const char* DD_INVALIDPIXELFORMAT;
  static const char* DD_INVALIDSURFACETYPE;
  static const char* DD_LOCKEDSURFACES;
  static const char* DD_NOALPHAHW;
  static const char* DD_NOCOOPERATIVELEVELSET;
  static const char* DD_NODIRECTDRAWHW;
  static const char* DD_NOCLIPPERATTACHED;
  static const char* DD_NOEMULATION;
  static const char* DD_NOEXEMODE;
  static const char* DD_NOFLIPHW;
  static const char* DD_NOMIPMAPHW;
  static const char* DD_NOOVERLAYHW;
  static const char* DD_NOTFOUND;  
  static const char* DD_NOTSUPPORT;
  static const char* DD_NOZBUFFERHW;
  static const char* DD_OUTOFMEMORY;
  static const char* DD_OUTOFVIDEOMEMORY;
  static const char* DD_PRIMARYSURFACEALREADYEXISTS;
  static const char* DD_SURFACELOST; 
  static const char* DD_SURFACESBUSY;
  static const char* DD_UNKNOWN_ERROR;
  static const char* DD_UNSUPPORTEDMODE;
  static const char* DD_WASSTILLDRAWING;
  static const char* INVALID_ID;
  static const char* INVALID_PARAM;
  static const char* OPEN_FAIL;
  static const char* SHORT_TIME_FAIL;
};
