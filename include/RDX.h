#pragma once

class RDX {
private:
	static RDX* instance;
	unsigned char* lpWorkPixel;
	LPDIRECTDRAW lpDD;
	LPDIRECTDRAWSURFACE lpBack;
	LPDIRECTDRAWSURFACE lpFront;
	LPDIRECTDRAWSURFACE lpWork;
	LPDIRECTDRAWCLIPPER lpClipper;
	RDX(void);
	int pixelFormat;
public:
	static const int R5G5B5;
	static const int R5G6B5;
	~RDX(void);
	void flip(void);
	static RDX* getInstance(void);
	LPDIRECTDRAWCLIPPER getDirectDrawCripper(void);
	LPDIRECTDRAWSURFACE getDirectDrawPrimarySurface(void);
	LPDIRECTDRAWSURFACE getDirectDrawSecondarySurface(void);
	LPDIRECTDRAWSURFACE getDirectDrawWorkSurface(void);
	int getPixelFormat(void);
	unsigned char* getWorkSurfacePixel(void);
	void init(bool fullscreen);
	void releaseRDX(void);
};
