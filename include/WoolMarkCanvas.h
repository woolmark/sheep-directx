#include <Canvas.h>
#include <MouseListener.h>
#include <Image.h>
#include <Sprite.h>
#include <Graphics.h>

class WoolMarkCanvas: public Canvas, public MouseListener {
private:
	Image* sheepImg;
	Image* backgroundImg;
	static const int SKY_COLOR = 0x9696ff;
	static const int GROUND_COLOR = 0x64ff64;
	static const int TEXT_COLOR = 0x000000;
	static const int SHEEP_MAX = 100;
	static const int WIDTH = 120;
	static const int HEIGHT = 120;
	Sprite* sheep;
	BOOL flag;
	int sheepPos[SHEEP_MAX][4];
	int sheepNumber;
	int sheepState;
public:
	void init(void);
	~WoolMarkCanvas(void);
	void paint(Graphics* g);
	void mouseEvent(int button);
	void clearSheepPosition(int* pos);
	void setSheepInitPosition(int* pos);
	int getJumpX(int y);
	void calc(void);
	void addSheep(void);
};
