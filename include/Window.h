#pragma once

#include <windows.h>

class Window {
private:
	HWND hWnd;
public:
	void createWindow(LPCTSTR lpClassName, LPCTSTR lpTitle, int x, int y,
			bool fullscreen);
};
