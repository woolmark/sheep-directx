#pragma once

class MousePosision {
private:
	static int x;
	static int y;
public:
	static int getMouseX(void);
	static int getMouseY(void);
	static void setMousePosision(int mX, int mY);
};
