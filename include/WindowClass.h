#pragma once

class WindowClass {
private:
	WNDCLASSEX* wcl;
public:
	WindowClass(void);
	~WindowClass(void);
	bool registerWindowClass(void);
	void setBackground(HBRUSH color);
	void setClassExtra(int extra);
	void setCursor(PCSTR pCursor);
	void setIcon(WORD hIcon);
	void setMenu(PCSTR pMenu);
	void setSmIcon(WORD hIconSm);
	void setWindowClassName(LPCTSTR wClassName);
	void setWindowExtra(int extra);
	void setWindowProc(void);
	void setWindowStyle(int style);static LRESULT CALLBACK WinProc(HWND,UINT,WPARAM,LPARAM);
};
