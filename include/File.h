#pragma once

#include <InputStream.h>
#include <RString.h>

class File: public InputStream {
private:
	int fileSize;
	int index;
	HANDLE handleFile;
	LPBYTE buffer;
	File(void);
public:
	static const DWORD READ;
	static const DWORD WRITE;
	static const DWORD READ_WRITE;
	File(RString string, DWORD mode = File::READ);
	~File(void);
	int getFileSize(void);
	BYTE readByte(void);
	int readByte(BYTE* buf, int offset, int length);
	int readInt(void);
};
