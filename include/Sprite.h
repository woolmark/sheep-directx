#pragma once

class Sprite {
private:
	int columns;
	int rows;
	int tileWidth;
	int tileHeight;
	Image* image;
	int currentCell;
public:
	Sprite(void);
	Sprite(Image* srcImage, int tWidth, int tHeight);
	~Sprite(void);
	Image* getImage(void);
	void setIndex(int index);
	int getTiledWidth(void);
	int getTiledHeight(void);
	int getCellX(void);
	int getCellY(void);
};
