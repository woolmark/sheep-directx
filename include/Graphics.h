#pragma once
#include <Image.h>
#include <Sprite.h>
#include <ImageButton.h>
#include <windows.h>
#include <ddraw.h>

class Graphics {
private:
	LPDIRECTDRAWSURFACE lpWork;
	static Graphics* instance;
	HDC hdc;
	int brushColor;
	bool fullscreen;
	unsigned char* lpWorkByte;
	int penColor;
	Graphics(bool mode);
public:
	~Graphics(void);
	void disposeGraphics(void);
	void drawImage(Image* img, int x, int y);
	void drawImageButton(ImageButton* imagebutton);
	void drawLine(int x1, int y1, int x2, int y2);
	void drawRect(int x, int y, int width, int height);
	void drawSprite(Sprite* sprite, int x, int y);
	void drawString(char* string, int x, int y);
	void fillRect(int x, int y, int width, int height);
	HDC getHDC(void);
	static Graphics* getInstance(bool mode);
	void setColor(int color);
};
