#pragma once

class InputStream {
public:
	virtual ~InputStream() {
	}
	virtual BYTE readByte(void) = 0;
	virtual int readByte(BYTE* buf, int offset, int length) = 0;
	virtual int readInt(void) = 0;
};
