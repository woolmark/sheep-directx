#pragma once

#include <Image.h>
#include <Sprite.h>

class ImageButton {
private:
	bool buttonAlive;
	int height;
	Image* img;
	Sprite* sp;
	int width;
	int x;
	int y;
	int type;
public:
	static const int TYPE_IMAGE;
	static const int TYPE_SPRITE;
	ImageButton(void);
	ImageButton(Image* image, int ix, int iy);
	ImageButton(Sprite* sprite, int ix, int iy);
	Image* getImage(void);
	Sprite* getSprite(void);
	int getType(void);
	int getX(void);
	int getY(void);
	bool isMouseOver(void);
	bool isAlive(void);
	void setAlive(bool flag);
};
