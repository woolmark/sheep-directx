#pragma once
#include <Image.h>
#include <RString.h>
#include <windows.h>

class Image {
private:
	static int imageType;
	static const int IMAGE_BMP;
	static const int IMAGE_RO555I;
	static const int IMAGE_RO565I;
	static const int IMAGE_ROI;
public:
	Image(void);
	virtual ~Image(void) {
	}
	;
	static Image* createImage(RString string);
	virtual LPBITMAPINFO getBitmapInfo(void) = 0;
	virtual LPBYTE getBytes(void) = 0;
	virtual int getHeight(void) = 0;
	virtual int getWidth(void) = 0;
	void release(void);
};
