#pragma once

#include <Graphics.h>
#include <KeyboardListener.h>
#include <MouseListener.h>

/**
 * Canvasクラス。画面描画キャンバスの基底クラス。
 * 描画用のキャンバスはこのクラスを継承し、paint()関数を
 * 実装する必要がある。
 */
class Canvas {
private:
	/** キーボードのListener */
	KeyboardListener* keyboardListener;

	/** マウスのListener */
	MouseListener* mouseListener;

protected:
	/**
	 * Canvasクラスのコンストラクタ。privateであるため、コンストラクタ
	 * が直接呼ばれることを禁止している。
	 */
	Canvas(void);
public:
	virtual ~Canvas(void) {
	}
	;
	/**
	 * Canvasの初期化などをするための関数。タイマーなどの初期化はここで
	 * 行う。仮想関数であるため、サブクラスで関数の中身は実装する必要がある。
	 */
	virtual void init(void) = 0;

	/**
	 * キーボードイベントをListener通知するための関数。
	 * @param event イベントのタイプ。
	 * @param vKey  押されたキーの種類。
	 */
	void keyBoardEvent(int event, int vKey);

	/**
	 * マウスイベントをListener通知するための関数。
	 * @param button マウスボタンのタイプとイベントの種類。
	 */
	void mouseEvent(int button);

	/**
	 * キャンバスに描画するための関数。仮想関数であるため、
	 * サブクラスで関数の中身は実装する必要がある。
	 * @param g 描画用のグラフィックス。
	 */
	virtual void paint(Graphics* g) = 0;

	/**
	 * 再描画をするための関数。この関数を呼び出すと画面が直ちに
	 * 再描画される。
	 */
	void repaint(void);

	/**
	 * KeyboadListenerを登録するための関数
	 * この関数に登録されたListenerにキーボードイベントが通知される。
	 * 既にListenerが登録されていた場合、登録されているListenerには
	 * イベントは通知されなくなる。
	 * @param listener キーボードイベントを通知させるリスナー。
	 */
	void setKeyboardListener(KeyboardListener* listener);

	/**
	 * MounseListenerを登録するための関数
	 * この関数に登録されたListenerにマウスイベントが通知される。
	 * 既にListenerが登録されていた場合、登録されているListenerには
	 * イベントは通知されなくなる。
	 * @param listener マウスイベントを通知させるリスナー。
	 */
	void setMouseListener(MouseListener* listener);
};
