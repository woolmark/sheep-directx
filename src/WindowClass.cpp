#include <BluePlanet.h>

WindowClass::WindowClass() {
	wcl = new WNDCLASSEX();
	wcl->cbSize = sizeof(WNDCLASSEX);
	wcl->hInstance = CAppInitializer::getInstance();
	wcl->lpszClassName = "";
	wcl->style = CS_DBLCLKS;
	wcl->cbClsExtra = 0;
	wcl->cbWndExtra = 0;
	wcl->hbrBackground = (HBRUSH) (COLOR_BACKGROUND);
	//wcl.hIcon = LoadIcon(CAppInitializer::getInstance(),NULL); // 標準アイコン
	//wcl.hIconSm = LoadIcon(CAppInitializer::getInstance(),NULL); // 小さいアイコン
	wcl->hCursor = LoadCursor(NULL, IDC_ARROW); // カーソルのスタイル
	wcl->lpszMenuName = NULL; // メニュー
}

WindowClass::~WindowClass() {
	delete wcl;
}

bool WindowClass::registerWindowClass(void) {
	if (RegisterClassEx(wcl)) {
		return true;
	}
	return false;
}

void WindowClass::setBackground(HBRUSH color) {
	wcl->hbrBackground = color;
}

void WindowClass::setClassExtra(int extra) {
	wcl->cbClsExtra = extra;
}

void WindowClass::setCursor(PCSTR pCursor) {
	wcl->hCursor = LoadCursor(NULL, pCursor);
}

void WindowClass::setIcon(WORD hIcon) {
	wcl->hIcon = LoadIcon(CAppInitializer::getInstance(),
	MAKEINTRESOURCE(hIcon));
}

void WindowClass::setMenu(PCSTR pMenu) {
	wcl->lpszMenuName = pMenu;
}

void WindowClass::setSmIcon(WORD hIconSm) {
	wcl->hIconSm = LoadIcon(CAppInitializer::getInstance(),
	MAKEINTRESOURCE(hIconSm));
}

void WindowClass::setWindowClassName(LPCTSTR wClassName) {
	wcl->lpszClassName = wClassName;
}

void WindowClass::setWindowExtra(int extra) {
	wcl->cbWndExtra = extra;
}

void WindowClass::setWindowProc(void) {
	wcl->lpfnWndProc = WinProc;
}

void WindowClass::setWindowStyle(int style) {

	wcl->style = style;
}

LRESULT CALLBACK WindowClass::WinProc(HWND hWnd, UINT msg, WPARAM wP,
		LPARAM lP) {
	switch (msg) {
	case WM_CREATE: {
		CAppInitializer::setHWND(hWnd);
		Display* display = Display::getDisplay();
		display->initDirectDraw();
		display->initCanvas();
	}
		break;

	case WM_PAINT: {
		Display* display = Display::getDisplay();
		display->startPaint(hWnd);
		display->endPaint();
	}
		break;

		/* マウスボタン処理 */

	case WM_LBUTTONDBLCLK:
	case WM_LBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_RBUTTONDBLCLK:
	case WM_RBUTTONDOWN:
	case WM_RBUTTONUP: {
		Display* display = Display::getDisplay();
		display->mouseEvent(msg);
	}
		break;

	case WM_MOUSEMOVE: {
		MousePosision::setMousePosision(LOWORD(lP), HIWORD(lP));
	}
		break;

		/* マウスボタン処理 ここまで*/

	case WM_KEYDOWN:
	case WM_KEYUP: {
		Display* display = Display::getDisplay();
		display->keyBoardEvent(msg, (int) wP);
	}
		break;

	case WM_DESTROY: //ウインドウの右上の×印が押されたときに
	{
		Display* display = Display::getDisplay();
		display->releaseDisplay();
		PostQuitMessage(0);
	}
		break;

	default:
		return DefWindowProc(hWnd, msg, wP, lP);
	}
	return 0;
}
