#include <BluePlanet.h>

void Window::createWindow(LPCTSTR lpClassName, LPCTSTR lpTitle, int x, int y,
		bool fullscreen) {

	Display* display = Display::getDisplay();
	display->setWindowSize(x, y);
	display->setFullScreenMode(fullscreen);

	if (display->isFullScreenMode()) {
		hWnd = CreateWindowEx(WS_EX_APPWINDOW, lpClassName, lpTitle,
		WS_POPUPWINDOW,
		CW_USEDEFAULT, CW_USEDEFAULT, x, y, NULL, NULL,
				CAppInitializer::getInstance(), NULL);
	} else {
		hWnd =
				CreateWindow(lpClassName, lpTitle,WS_OVERLAPPEDWINDOW,
						CW_USEDEFAULT,CW_USEDEFAULT, x + 8, y + 27, HWND_DESKTOP, NULL, CAppInitializer::getInstance(), NULL);
	}

	CAppInitializer::setHWND(hWnd);
	ShowWindow(hWnd, CAppInitializer::winMode());
	UpdateWindow(hWnd);
}
