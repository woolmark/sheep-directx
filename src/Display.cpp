#include <BluePlanet.h>

int Display::width = 0;
int Display::height = 0;
Display* Display::instance = NULL;

const int Display::LEFT_MOUSE_DOUBLECLICK = WM_LBUTTONDBLCLK;
const int Display::LEFT_MOUSE_DOWN = WM_LBUTTONDOWN;
const int Display::LEFT_MOUSE_UP = WM_LBUTTONUP;
const int Display::RIGHT_MOUSE_DOUBLECLICK = WM_RBUTTONDBLCLK;
const int Display::RIGHT_MOUSE_DOWN = WM_RBUTTONDOWN;
const int Display::RIGHT_MOUSE_UP = WM_RBUTTONUP;
const int Display::KEYBOARD_DOWN = WM_KEYDOWN;
const int Display::KEYBOARD_UP = WM_KEYUP;

Display::Display(void) {
	canvas = NULL;
	directX = NULL;
	fullscreen = true;
	g = NULL;
	lpBack = NULL;
	lpClipper = NULL;
	lpFront = NULL;
	lpWork = NULL;
}

void Display::endPaint(void) {
	RECT rec = { 0, 0, Display::getWidth(), Display::getHeight() };
	lpBack->BltFast(0, 0, lpWork, &rec, DDBLTFAST_NOCOLORKEY | DDBLTFAST_WAIT);
	directX->flip();
}

Display* Display::getDisplay(void) {
	if (instance == NULL) {
		instance = new Display();
	}
	return instance;
}

int Display::getHeight(void) {
	return height;
}

int Display::getWidth(void) {
	return width;
}

void Display::releaseDisplay(void) {
	delete instance;
	directX->releaseRDX();
	g->disposeGraphics();
}

void Display::initDirectDraw(void) {
	directX = RDX::getInstance();
	directX->init(fullscreen);
	lpBack = directX->getDirectDrawSecondarySurface();
	lpFront = directX->getDirectDrawPrimarySurface();
	lpWork = directX->getDirectDrawWorkSurface();
	lpClipper = directX->getDirectDrawCripper();
	g = Graphics::getInstance(fullscreen);
}

bool Display::isFullScreenMode(void) {
	return fullscreen;
}

void Display::keyBoardEvent(int event, int vKey) {
	canvas->keyBoardEvent(event, vKey);
}

void Display::mouseEvent(int button) {
	canvas->mouseEvent(button);
}

void Display::setCurrent(Canvas* screen) {
	canvas = screen;
}

void Display::setFullScreenMode(bool mode) {
	fullscreen = mode;
}

void Display::setWindowSize(int x, int y) {
	width = x;
	height = y;
}

void Display::initCanvas(void) {
	canvas->init();
}

void Display::startPaint(HWND hWnd) {
	canvas->paint(g);

	if (!fullscreen) {
		RECT rc;
		RECT DisplayRect = { 0, 0, Display::getWidth(), Display::getHeight() };
		GetClientRect(hWnd, &rc);
		ClientToScreen(hWnd, (LPPOINT) &rc);
		ClientToScreen(hWnd, (LPPOINT) &rc + 1);
		int result = lpFront->Blt(&rc, lpWork, &DisplayRect,
		DDBLT_ASYNC | DDBLT_WAIT, NULL);
		if (result != DD_OK) {
			switch (result) {
			case DDERR_GENERIC:
				WARNING("1")
				;
				break;
			case DDERR_INVALIDCLIPLIST:
				WARNING("2")
				;
				break;
			case DDERR_INVALIDOBJECT:
				WARNING("3")
				;
				break;
			case DDERR_INVALIDPARAMS:
				WARNING("4")
				;
				break;
			case DDERR_INVALIDRECT:
				WARNING("5")
				;
				break;
			case DDERR_NOALPHAHW:
				WARNING("6")
				;
				break;
			case DDERR_NOBLTHW:
				WARNING("7")
				;
				break;
			case DDERR_NOCLIPLIST:
				WARNING("8")
				;
				break;
			case DDERR_NODDROPSHW:
				WARNING("9")
				;
				break;
			case DDERR_NOMIRRORHW:
				WARNING("10")
				;
				break;
			case DDERR_NORASTEROPHW:
				WARNING("11")
				;
				break;
			case DDERR_NOROTATIONHW:
				WARNING("12")
				;
				break;
			case DDERR_NOSTRETCHHW:
				WARNING("13")
				;
				break;
			case DDERR_NOZBUFFERHW:
				WARNING("14")
				;
				break;
			case DDERR_SURFACEBUSY:
				WARNING("15")
				;
				break;
			case DDERR_SURFACELOST:
				WARNING("16")
				;
				break;
			case DDERR_UNSUPPORTED:
				WARNING("17")
				;
				break;
			}
		}
	}
}
