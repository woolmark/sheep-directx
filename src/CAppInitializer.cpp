#include <BluePlanet.h>

HINSTANCE CAppInitializer::hThisInst;
HINSTANCE CAppInitializer::hPrevInst;
LPSTR CAppInitializer::lpszArgs;
HWND CAppInitializer::hWnd;
int CAppInitializer::nWinMode;

LPSTR CAppInitializer::getCommandLine(void) {
	return lpszArgs;
}

HWND CAppInitializer::getHWND(void) {
	return hWnd;
}

HINSTANCE CAppInitializer::getInstance(void) {
	return hThisInst;
}

HINSTANCE CAppInitializer::getPrevInstance(void) {
	return hPrevInst;
}

void CAppInitializer::init(HINSTANCE hInstance, HINSTANCE hPrevInstance,
		LPSTR lpCmdLine, int nCmdShow) {
	hThisInst = hInstance;
	hPrevInst = hPrevInstance;
	lpszArgs = lpCmdLine;
	nWinMode = nCmdShow;
}

void CAppInitializer::setHWND(HWND HWnd) {
	hWnd = HWnd;
}

int CAppInitializer::winMode() {
	return nWinMode;
}

