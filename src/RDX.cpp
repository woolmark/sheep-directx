#include <BluePlanet.h>

RDX* RDX::instance = NULL;
const int RDX::R5G5B5 = 0;
const int RDX::R5G6B5 = 1;

RDX::RDX(void) {
	pixelFormat = 0;
	lpWorkPixel = NULL;
	lpDD = NULL;
	lpFront = NULL;
	lpBack = NULL;
	lpWork = NULL;
	lpClipper = NULL;
}

RDX::~RDX(void) {
	if (lpClipper != NULL)
		lpClipper->Release();
	if (lpFront != NULL)
		lpFront->Release();
	if (lpWork != NULL)
		lpWork->Release();
	if (lpBack != NULL)
		lpBack->Release();
	if (lpDD != NULL)
		lpDD->Release();
}

void RDX::flip(void) {
	RECT rec = { 0, 0, Display::getWidth(), Display::getHeight() };
	lpFront->BltFast(0, 0, lpBack, &rec, DDBLTFAST_NOCOLORKEY | DDBLTFAST_WAIT);
}

RDX* RDX::getInstance(void) {
	if (instance == NULL) {
		instance = new RDX();
	}
	return instance;
}

LPDIRECTDRAWCLIPPER RDX::getDirectDrawCripper(void) {
	return lpClipper;
}

LPDIRECTDRAWSURFACE RDX::getDirectDrawPrimarySurface(void) {
	return lpFront;
}

LPDIRECTDRAWSURFACE RDX::getDirectDrawSecondarySurface(void) {
	return lpBack;
}

LPDIRECTDRAWSURFACE RDX::getDirectDrawWorkSurface(void) {
	return lpWork;
}

int RDX::getPixelFormat(void) {
	return pixelFormat;
}

unsigned char* RDX::getWorkSurfacePixel(void) {
	return lpWorkPixel;
}

void RDX::init(bool fullscreen) {
	DDSURFACEDESC ddsd;

	HRESULT result = DirectDrawCreate((GUID*) DDCREATE_EMULATIONONLY, &lpDD,
	NULL);
	if (result != DD_OK) {
		switch (result) {
		case E_OUTOFMEMORY:
			WARNING(Error::DD_OUTOFMEMORY)
			;
			break;
		case DDERR_GENERIC:
			WARNING(Error::DD_UNKNOWN_ERROR)
			;
			break;
		case DDERR_UNSUPPORTED:
			WARNING(Error::DD_NOTSUPPORT)
			;
			break;
		case DDERR_DIRECTDRAWALREADYCREATED:
			WARNING(Error::DD_ALREADY)
			;
			break;
		case DDERR_INVALIDDIRECTDRAWGUID:
			WARNING(Error::INVALID_ID)
			;
			break;
		case DDERR_INVALIDPARAMS:
			WARNING(Error::INVALID_PARAM)
			;
			break;
		case DDERR_NODIRECTDRAWHW:
			WARNING(Error::DD_NOTSUPPORT)
			;
			break;
		}
		return;
	}

	if (fullscreen) {
		result = lpDD->SetCooperativeLevel(CAppInitializer::getHWND(),
		DDSCL_FULLSCREEN | DDSCL_EXCLUSIVE);
	} else {
		result = lpDD->SetCooperativeLevel(CAppInitializer::getHWND(),
		DDSCL_NORMAL);
	}

	if (result != DD_OK) {
		switch (result) {
		case DDERR_EXCLUSIVEMODEALREADYSET:
			WARNING(Error::DD_EXE_ALREDY)
			;
			break;
		case DDERR_HWNDALREADYSET:
			WARNING(Error::DD_HWND_ALREDY)
			;
			break;
		case DDERR_HWNDSUBCLASSED:
			WARNING(Error::DD_HWND_SUBCLASS)
			;
			break;
		case DDERR_INVALIDOBJECT:
			WARNING(Error::DD_INVALID_OBJ)
			;
			break;
		case DDERR_INVALIDPARAMS:
			WARNING(Error::INVALID_PARAM)
			;
			break;
		case DDERR_OUTOFMEMORY:
			WARNING(Error::DD_OUTOFMEMORY)
			;
			break;
		}
		return;
	}

	if (fullscreen) {
		result = lpDD->SetDisplayMode(Display::getWidth(), Display::getHeight(),
				16);
	}

	if (result != DD_OK) {
		switch (result) {
		case DDERR_GENERIC:
			WARNING(Error::DD_UNKNOWN_ERROR)
			;
			break;
		case DDERR_INVALIDMODE:
			WARNING(Error::DD_INVALID_MODE)
			;
			break;
		case DDERR_INVALIDOBJECT:
			WARNING(Error::DD_INVALID_OBJ)
			;
			break;
		case DDERR_INVALIDPARAMS:
			WARNING(Error::INVALID_PARAM)
			;
			break;
		case DDERR_LOCKEDSURFACES:
			WARNING(Error::DD_LOCKEDSURFACES)
			;
			break;
		case DDERR_NOEXCLUSIVEMODE:
			WARNING(Error::DD_NOEXEMODE)
			;
			break;
		case DDERR_SURFACEBUSY:
			WARNING(Error::DD_SURFACESBUSY)
			;
			break;
		case DDERR_UNSUPPORTED:
			WARNING(Error::DD_NOTSUPPORT)
			;
			break;
		case DDERR_UNSUPPORTEDMODE:
			WARNING(Error::DD_NOEXEMODE)
			;
			break;
		case DDERR_WASSTILLDRAWING:
			WARNING(Error::DD_WASSTILLDRAWING)
			;
			break;
		}
		return;
	}

	ZeroMemory(&ddsd, sizeof(ddsd));
	ddsd.dwSize = sizeof(ddsd);

	if (fullscreen) {
		ddsd.dwFlags = DDSD_CAPS | DDSD_BACKBUFFERCOUNT;
		ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE | DDSCAPS_FLIP
				| DDSCAPS_COMPLEX;
		ddsd.dwBackBufferCount = 1;
	} else {
		ddsd.dwFlags = DDSD_CAPS;
		ddsd.ddsCaps.dwCaps = DDSCAPS_PRIMARYSURFACE;
	}

	result = lpDD->CreateSurface(&ddsd, &lpFront, NULL);

	if (result != DD_OK) {
		switch (result) {
		case DDERR_INCOMPATIBLEPRIMARY:
			WARNING(Error::DD_INCOMPATIBLEPRIMARY)
			;
			break;
		case DDERR_INVALIDCAPS:
			WARNING(Error::DD_INVALIDCAPS)
			;
			break;
		case DDERR_INVALIDOBJECT:
			WARNING(Error::DD_INVALID_OBJ)
			;
			break;
		case DDERR_INVALIDPARAMS:
			WARNING(Error::INVALID_PARAM)
			;
			break;
		case DDERR_INVALIDPIXELFORMAT:
			WARNING(Error::DD_INVALIDPIXELFORMAT)
			;
			break;
		case DDERR_NOALPHAHW:
			WARNING(Error::DD_NOALPHAHW)
			;
			break;
		case DDERR_NOCOOPERATIVELEVELSET:
			WARNING(Error::DD_NOCOOPERATIVELEVELSET)
			;
			break;
		case DDERR_NODIRECTDRAWHW:
			WARNING(Error::DD_NODIRECTDRAWHW)
			;
			break;
		case DDERR_NOEMULATION:
			WARNING(Error::DD_NOEMULATION)
			;
			break;
		case DDERR_NOEXCLUSIVEMODE:
			WARNING(Error::DD_NOEXEMODE)
			;
			break;
		case DDERR_NOFLIPHW:
			WARNING(Error::DD_NOFLIPHW)
			;
			break;
		case DDERR_NOMIPMAPHW:
			WARNING(Error::DD_NOFLIPHW)
			;
			break;
		case DDERR_NOOVERLAYHW:
			WARNING(Error::DD_NOOVERLAYHW)
			;
			break;
		case DDERR_NOZBUFFERHW:
			WARNING(Error::DD_NOZBUFFERHW)
			;
			break;
		case DDERR_OUTOFMEMORY:
			WARNING(Error::DD_OUTOFMEMORY)
			;
			break;
		case DDERR_OUTOFVIDEOMEMORY:
			WARNING(Error::DD_OUTOFVIDEOMEMORY)
			;
			break;
		case DDERR_PRIMARYSURFACEALREADYEXISTS:
			WARNING(Error::DD_PRIMARYSURFACEALREADYEXISTS)
			;
			break;
		case DDERR_UNSUPPORTEDMODE:
			WARNING(Error::DD_UNSUPPORTEDMODE)
			;
			break;
		}
		return;
	}

	DDSURFACEDESC ddsd2 = { sizeof(ddsd2) };
	ddsd2.ddsCaps.dwCaps = DDSD_PIXELFORMAT;
	result = lpFront->GetSurfaceDesc(&ddsd2);

	if (result != DD_OK) {
		switch (result) {
		case DDERR_INVALIDOBJECT:
			WARNING(Error::DD_INVALID_OBJ)
			;
			break;
		case DDERR_INVALIDPARAMS:
			WARNING(Error::INVALID_PARAM)
			;
			break;
		}
		return;
	}

	if (ddsd2.ddpfPixelFormat.dwGBitMask == 0x7E0) {
		pixelFormat = RDX::R5G6B5;
	} else {
		pixelFormat = RDX::R5G5B5;
	}

	if (fullscreen) {
		ddsd.ddsCaps.dwCaps = DDSCAPS_BACKBUFFER | DDSCAPS_SYSTEMMEMORY;
		result = lpFront->GetAttachedSurface(&ddsd.ddsCaps, &lpBack);

		if (result != DD_OK) {
			switch (result) {
			case DDERR_INVALIDOBJECT:
				WARNING(Error::DD_INVALID_OBJ)
				;
				break;
			case DDERR_INVALIDPARAMS:
				WARNING(Error::INVALID_PARAM)
				;
				break;
			case DDERR_NOTFOUND:
				WARNING(Error::DD_NOTFOUND)
				;
				break;
			case DDERR_SURFACELOST:
				WARNING(Error::DD_SURFACELOST)
				;
				break;
			}
			return;
		}

		ZeroMemory(&ddsd, sizeof(ddsd));
		ddsd.dwSize = sizeof(ddsd);
		ddsd.dwFlags = DDSD_CAPS | DDSD_WIDTH | DDSD_HEIGHT;
		ddsd.ddsCaps.dwCaps = DDSCAPS_OFFSCREENPLAIN | DDSCAPS_SYSTEMMEMORY;
		ddsd.dwWidth = Display::getWidth();
		ddsd.dwHeight = Display::getHeight();

		result = lpDD->CreateSurface(&ddsd, &lpWork, NULL);
		if (result != DD_OK) {
			switch (result) {
			case DDERR_INVALIDOBJECT:
				WARNING(Error::DD_INVALID_OBJ)
				;
				break;
			case DDERR_INVALIDPARAMS:
				WARNING(Error::INVALID_PARAM)
				;
				break;
			case DDERR_NOTFOUND:
				WARNING(Error::DD_NOTFOUND)
				;
				break;
			case DDERR_SURFACELOST:
				WARNING(Error::DD_SURFACELOST)
				;
				break;
			}
			return;
		}
		lpWork->Lock(NULL, &ddsd, DDLOCK_WAIT, NULL);
		lpWorkPixel = (LPBYTE) ddsd.lpSurface;
		lpWork->Unlock(NULL);
	} else {
		result = lpDD->CreateClipper(0, &lpClipper, NULL);
		if (result != DD_OK) {
			switch (result) {
			case DDERR_INVALIDOBJECT:
				WARNING(Error::DD_INVALID_OBJ)
				;
				break;
			case DDERR_INVALIDPARAMS:
				WARNING(Error::INVALID_PARAM)
				;
				break;
			case DDERR_NOCOOPERATIVELEVELSET:
				WARNING(Error::DD_NOCOOPERATIVELEVELSET)
				;
				break;
			case DDERR_OUTOFMEMORY:
				WARNING(Error::DD_OUTOFMEMORY)
				;
				break;
			}
			return;
		}

		result = lpClipper->SetHWnd(0, CAppInitializer::getHWND());
		if (result != DD_OK) {
			switch (result) {
			case DDERR_INVALIDOBJECT:
				WARNING(Error::DD_INVALID_OBJ)
				;
				break;
			case DDERR_INVALIDPARAMS:
				WARNING(Error::INVALID_PARAM)
				;
				break;
			case DDERR_NOCOOPERATIVELEVELSET:
				WARNING(Error::DD_NOCOOPERATIVELEVELSET)
				;
				break;
			case DDERR_OUTOFMEMORY:
				WARNING(Error::DD_OUTOFMEMORY)
				;
				break;
			}
			return;
		}

		result = lpFront->SetClipper(lpClipper);
		if (result != DD_OK) {
			switch (result) {
			case DDERR_INVALIDOBJECT:
				WARNING(Error::DD_INVALID_OBJ)
				;
				break;
			case DDERR_INVALIDPARAMS:
				WARNING(Error::INVALID_PARAM)
				;
				break;
			case DDERR_INVALIDSURFACETYPE:
				WARNING(Error::DD_INVALIDSURFACETYPE)
				;
				break;
			case DDERR_NOCLIPPERATTACHED:
				WARNING(Error::DD_NOCLIPPERATTACHED)
				;
				break;
			}
			return;
		}

		ZeroMemory(&ddsd, sizeof(ddsd));
		ddsd.dwSize = sizeof(ddsd);
		ddsd.dwFlags = DDSD_CAPS | DDSD_WIDTH | DDSD_HEIGHT;
		ddsd.dwWidth = Display::getWidth();
		ddsd.dwHeight = Display::getHeight();
		ddsd.ddsCaps.dwCaps = DDSCAPS_OFFSCREENPLAIN | DDSCAPS_SYSTEMMEMORY
				| DDSCAPS_3DDEVICE | DDSCAPS_OWNDC;
		result = lpDD->CreateSurface(&ddsd, &lpBack, 0);

		lpWork = lpBack;
		lpWork->AddRef();
		lpWork->Lock(NULL, &ddsd, DDLOCK_WAIT, NULL);
		lpWorkPixel = (LPBYTE) ddsd.lpSurface;
		lpWork->Unlock(NULL);
		if (result != DD_OK) {
			switch (result) {
			case DDERR_INVALIDOBJECT:
				WARNING(Error::DD_INVALID_OBJ)
				;
				break;
			case DDERR_INVALIDPARAMS:
				WARNING(Error::INVALID_PARAM)
				;
				break;
			case DDERR_NOTFOUND:
				WARNING(Error::DD_NOTFOUND)
				;
				break;
			case DDERR_SURFACELOST:
				WARNING(Error::DD_SURFACELOST)
				;
				break;
			}
			return;
		}
	}
}

void RDX::releaseRDX(void) {
	delete instance;
}
