#include <BluePlanet.h>

const char* Error::DD_ALREADY = "DirectDrawExオブジェクトが既に作成されています。";
const char* Error::DD_EXE_ALREDY = "すでにその協調モードは実行されています。";
const char* Error::DD_HWND_ALREDY = "すでにハンドルが協調モードで実行されています。";
const char* Error::DD_HWND_SUBCLASS = "サブクラスのハンドルが指定されました。";
const char* Error::DD_INVALID_OBJ = "不正なDirextDrawオブジェクトで呼び出されました。";
const char* Error::DD_INVALID_MODE = "不正な協調モードで呼び出されました。";
const char* Error::DD_INCOMPATIBLEPRIMARY =
		"プライマリ サーフェスの作成要求が既存のプライマリ サーフェスと一致していません。";
const char* Error::DD_INVALIDCAPS = "コールバック関数に渡された1つ以上の能\力ビットが不正です";
const char* Error::DD_INVALIDPIXELFORMAT = "指定されたピクセル フォーマットが無効です。";
const char* Error::DD_INVALIDSURFACETYPE = "不正なサーフェースのタイプです。";
const char* Error::DD_LOCKEDSURFACES = "サーフェースがロックされています。";
const char* Error::DD_NOALPHAHW =
		"アルファ アクセラレーション ハードウェアが存在しないか、利用できないため、要求された処理が失敗しました。";
const char* Error::DD_NOCOOPERATIVELEVELSET =
		"サーフェスの作成前に IDirectDraw2::SetCooperativeLevelメソ\ッドが呼び出されませんでした。";
const char* Error::DD_NODIRECTDRAWHW =
		"ハードウェア専用 DirectDraw オブジェクトを作成できない。ドライバはハードウェアをサポートしていません。";
const char* Error::DD_NOCLIPPERATTACHED = "クリッパーがアタッチされていません。";
const char* Error::DD_NOEMULATION = "ソ\フトウェア エミュレーションが利用できません。";
const char* Error::DD_NOEXEMODE = "EXCLUSIVEモードでありません。";
const char* Error::DD_NOFLIPHW = "表\示されるサーフェスをフリッピングできません。";
const char* Error::DD_NOMIPMAPHW =
		"ミップマップ テクスチャをマッピングするハードウェアが存在しないか、利用できないため、処理を実行できません。";
const char* Error::DD_NOOVERLAYHW = "オーバーレイ ハードウェアが存在しないか、利用できないため、処理を実行できません";
const char* Error::DD_NOTFOUND = "リクエストしたオブジェクトがみつかりません。";
const char* Error::DD_NOTSUPPORT = "DirextDrawをサポートしてません。";
const char* Error::DD_NOZBUFFERHW =
		"Z バッファに対するハードウェア サポートがないため、ディスプレイ メモリでの Z バッファ作成、あるいは Z バッファを使用したブリットが実行できません。";
const char* Error::DD_OUTOFVIDEOMEMORY =
		"DirectDraw が利用できるディスプレイメモリが足りないため、処理を実行できません。";
const char* Error::DD_OUTOFMEMORY = "メモリが足りません。";
const char* Error::DD_PRIMARYSURFACEALREADYEXISTS =
		"アプリケーションは既にプライマリ サーフェスを作成していません。";
const char* Error::DD_SURFACELOST = "サーフェースが失われました。";
const char* Error::DD_SURFACESBUSY = "サーフェースがビジーです。";
const char* Error::DD_UNKNOWN_ERROR = "不明なエラーです。";
const char* Error::DD_UNSUPPORTEDMODE = "処理がサポートされてません。";
const char* Error::DD_WASSTILLDRAWING = "ビデオ ハードウェアの状態が不適切です。";
const char* Error::INVALID_ID = "DirectDrawExオブジェクトの識別IDが無効です。";
const char* Error::INVALID_PARAM = "不正なパラメータを渡されました。";
const char* Error::OPEN_FAIL = "ファイルがオープンできません。:";
const char* Error::SHORT_TIME_FAIL = "ショートタイマーの初期化に失敗しました。";

