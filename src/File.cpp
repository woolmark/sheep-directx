#include <BluePlanet.h>

const DWORD File::READ = GENERIC_READ;
const DWORD File::WRITE = GENERIC_WRITE;
const DWORD File::READ_WRITE = GENERIC_READ | GENERIC_WRITE;

File::File(void) {
	handleFile = NULL;
	fileSize = 0;
	buffer = NULL;
	index = 0;
}

File::~File(void) {
	delete buffer;
}

File::File(RString string, DWORD mode) {
	DWORD dwResult;
	index = 0;
	handleFile = CreateFile(string, File::READ, 0, NULL, OPEN_EXISTING,
	FILE_ATTRIBUTE_NORMAL, NULL);
	if (INVALID_HANDLE_VALUE == handleFile) {
		WARNING(Error::OPEN_FAIL + string);
	}
	fileSize = (int) GetFileSize(handleFile, NULL);
	buffer = new BYTE[fileSize];
	ReadFile(handleFile, buffer, fileSize, &dwResult, NULL);
	CloseHandle(handleFile);
}

int File::getFileSize(void) {
	return fileSize;
}

BYTE File::readByte(void) {
	return buffer[index++];
}

int File::readByte(BYTE* buf, int offset, int length) {
	int start = index;
	for (int i = offset; i < length - offset; i++, index++) {
		buf[i] = buffer[index];
	}

	return index - start;
}

int File::readInt(void) {
	int temp = (int) buffer[index] + (int) (buffer[index + 1] << 8)
			+ (int) (buffer[index + 2] << 16) + (int) (buffer[index + 3] << 24);
	index = index + 4;
	return temp;
}

