#include <BluePlanet.h>

const int Image::IMAGE_BMP = 0;
const int Image::IMAGE_ROI = 1;
const int Image::IMAGE_RO555I = 2;
const int Image::IMAGE_RO565I = 3;

int Image::imageType = -1;

Image::Image(void) {
}

Image* Image::createImage(RString string) {
	if (string.subString(string.length() - 4, string.length()).equals(".bmp")) {
		imageType = IMAGE_BMP;
		return new BMPImage(string);
	}
	return NULL;
}

void Image::release(void) {
	switch (imageType) {
	case IMAGE_BMP:
		((BMPImage*) this)->release();
		break;
	default:
		break;
	}
}
