#include <BluePlanet.h>

Graphics* Graphics::instance = NULL;

Graphics::Graphics(bool mode) {
	hdc = NULL;
	fullscreen = mode;
	brushColor = 0;
	penColor = 0;
	RDX* directX = RDX::getInstance();
	lpWork = directX->getDirectDrawWorkSurface();
	lpWorkByte = directX->getWorkSurfacePixel();
}

Graphics::~Graphics(void) {
	DeleteObject(hdc);
}

void Graphics::disposeGraphics(void) {
	delete instance;
}

void Graphics::drawImage(Image* img, int x, int y) {
	unsigned char* src = img->getBytes();

	int width = img->getWidth();
	int height = img->getHeight();
	int k = 0;
	int windowWidth = Display::getWidth();
	int windowheight = Display::getHeight();

	for (int i = y + height; i > y; i--) {
		for (int j = x; j < x + width; j++) {
			int index = 4 * (j + windowWidth * i);
			if ((i < windowheight) && (j < windowWidth) && (i >= 0) && (j >= 0)
					&& (index >= 0)
					&& (index < windowWidth * windowheight * 4)) {

				if (src[k] == 0x4c && src[k + 1] == 0xb1
						&& src[k + 2] == 0x22) {
					k = k + 3;
				} else {
					lpWorkByte[index] = src[k++];
					lpWorkByte[index + 1] = src[k++];
					lpWorkByte[index + 2] = src[k++];
					lpWorkByte[index + 3] = 0;
				}
			} else {
				//k = k + 2;
			}
		}
	}
}

void Graphics::drawImageButton(ImageButton* imagebutton) {
	if (!imagebutton->isAlive())
		return;

	if (imagebutton->getType() == ImageButton::TYPE_IMAGE) {
		Image* img = imagebutton->getImage();
		drawImage(img, imagebutton->getX(), imagebutton->getY());
	} else if (imagebutton->getType() == ImageButton::TYPE_SPRITE) {
		Sprite* sprite = imagebutton->getSprite();
		drawSprite(sprite, imagebutton->getX(), imagebutton->getY());
	} else {
	}
}

void Graphics::drawLine(int x1, int y1, int x2, int y2) {
	lpWork->GetDC(&hdc);

	HPEN hPen = CreatePen(PS_SOLID, 1, penColor);
	SelectObject(hdc, hPen);
	MoveToEx(hdc, x1, y1, NULL);
	LineTo(hdc, x2, y2);
	DeleteObject(hPen);

	lpWork->ReleaseDC(hdc);
}

void Graphics::drawRect(int x, int y, int width, int height) {
	lpWork->GetDC(&hdc);

	HPEN hPen = CreatePen(PS_SOLID, 1, penColor);
	SelectObject(hdc, hPen);
	SelectObject(hdc, GetStockObject(NULL_BRUSH));
	Rectangle(hdc, x, y, x + width, y + height);
	DeleteObject(hPen);

	lpWork->ReleaseDC(hdc);
}

void Graphics::drawSprite(Sprite* sprite, int x, int y) {
	Image* img = sprite->getImage();
	unsigned char* src = img->getBytes();

	int imageX = sprite->getCellX();
	int imageY = sprite->getCellY();
	int width = sprite->getTiledWidth();
	int height = sprite->getTiledHeight();
	int imageWidth = img->getWidth();
	//int imageHeight = img->getHeight();
	int k = 0;
	int windowWidth = Display::getWidth();
	int windowheight = Display::getHeight();

	if (imageX == -1 || imageY == -1)
		return;
	unsigned char* buffer = new unsigned char[width * height * 3];

	for (int i = imageY; i < imageY + height; i++) {
		for (int j = imageX; j < imageX + width; j++) {
			int index = 3 * (j + imageWidth * i);
			buffer[k++] = src[index];
			buffer[k++] = src[index + 1];
			buffer[k++] = src[index + 2];
		}
	}
	k = 0;
	for (int i = y + height; i > y; i--) {
		for (int j = x; j < x + width; j++) {
			int index = 4 * (j + windowWidth * i);
			if ((i < windowheight) && (j < windowWidth) && (i >= 0) && (j >= 0)
					&& (index >= 0)
					&& (index < windowWidth * windowheight * 4)) {

				if (buffer[k] == 0x4c && buffer[k + 1] == 0xb1
						&& buffer[k + 2] == 0x22) {
					k = k + 3;
				} else {
					lpWorkByte[index] = buffer[k++];
					lpWorkByte[index + 1] = buffer[k++];
					lpWorkByte[index + 2] = buffer[k++];
					lpWorkByte[index + 3] = 0;
				}

			} else {
				k = k + 2;
			}
		}
	}

	delete buffer;
}

void Graphics::drawString(char* string, int x, int y) {
	HFONT hFont;

	lpWork->GetDC(&hdc);
	SetBkMode(hdc, TRANSPARENT);

	hFont = CreateFont(12, 0, 0, 0, FW_NORMAL, 0, 0, 0, DEFAULT_CHARSET,
	OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
	DEFAULT_QUALITY, DEFAULT_PITCH, "MS P�S�V�b�N");
	SelectObject(hdc, hFont);
	TextOut(hdc, x, y, string, strlen(string));
	DeleteObject(hFont);
	lpWork->ReleaseDC(hdc);
}

void Graphics::fillRect(int x, int y, int width, int height) {
	lpWork->GetDC(&hdc);

	HPEN hPen = CreatePen(PS_SOLID, 1, penColor);
	HBRUSH hBrush = CreateSolidBrush(brushColor);
	SelectObject(hdc, hPen);
	SelectObject(hdc, hBrush);
	Rectangle(hdc, x, y, x + width, y + height);
	DeleteObject(hPen);
	DeleteObject(hBrush);

	lpWork->ReleaseDC(hdc);
}

HDC Graphics::getHDC(void) {
	return hdc;
}

Graphics* Graphics::getInstance(bool mode) {
	if (instance == NULL) {
		instance = new Graphics(mode);
	}

	return instance;
}

void Graphics::setColor(int color) {
	WORD r, g, b;
	r = color >> 16;
	g = (color & 0x0000ff00) >> 8;
	b = color & 0x000000ff;

	brushColor = RGB(r, g, b);
	penColor = RGB(r, g, b);
}
